<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api {

    public function call($path = '', $params = array(), $method = 'GET'){

        $CI =& get_instance();

        $CI->load->config('api_config', true);

        $ch = curl_init();

        if( isset($params['api_server'])){
            switch ($params['api_server']) {
                case 'stg':
                    $url = 'https://hrtechapi.stg.plus.enigma.bz/'.$path;
                    break;
                case 'tst':
                    $url = 'https://hrtechapi.test.plus.enigma.bz/'.$path;
                    break;
                case 'dev':
                    $url = 'https://hrtechapi.dev.plus.enigma.bz/'.$path;
                    break;
                default:
                    $url = 'https://hrtechapi.dev.plus.enigma.bz/'.$path;
                    break;
            }
        }else{
            $url = $CI->config->item('api_server', 'api_config').$path;
        }

        $body = '';

        switch($method){
            case 'GET':
                $url .= "?".http_build_query($params['body']);
            break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params['body']));
                $body = http_build_query($params['body']);
            break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, count($params['body']));
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params['body']));
                $body = http_build_query($params['body']);
            break;
            case 'FILE':
                if(!isset($params['files']) || empty($params['files'])){
                    return false;
                }

                $file = $params['files'];
                unset($params['files']);
                $cfile = new CURLFile($file['tmp_name'], $file['type'], $file['name']);
                $body = array('file' => $cfile);

                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            break;

        }

        curl_setopt($ch, CURLINFO_HEADER_OUT,true);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_URL, $url);

        if(isset($params['show_header'])){
            curl_setopt($ch, CURLOPT_HEADER, true);
        }else{
            curl_setopt($ch, CURLOPT_HEADER, false);
        }

        if(isset($params['headers'])){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);
        }

        if(isset($params['request_log'])&&$params['request_log']){
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        }

        $api_result = array(
            'response'      => curl_exec($ch)
        );

        if(isset($params['request_log'])&&$params['request_log']){
            $api_result['request'] = 'EFFECTIVE-URL: '.curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)."\n".
                                     curl_getinfo($ch, CURLINFO_HEADER_OUT).
                                     'Request-Body: '.var_export($body, true);
        }

        echo curl_getinfo($curl, CURLINFO_HEADER_OUT);

        curl_close($ch);

        return $api_result;

    }

}