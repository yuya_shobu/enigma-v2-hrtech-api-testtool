<?php $this->load->view('eapi/common/header.php');?>
<?php $this->load->view('eapi/common/sidebar.php');?>

<div class="title">利用実績ダウンロードAPI</div>
<div class="conf">
    <div class="conf1">
        <?php echo form_open_multipart(base_url('eapi/download/exec'), array('method' => 'post'));?>
        <label>企業コード</label><br>
        <input type="text" name="company_code" value="" required>
        <?php echo empty(form_error('company_code'))?"<br>":form_error('company_code'); ?><br>

        <label>ダウンロード対象年</label><br>
        <div class="select">
        <select name="year" required>
        <?php for($y = 2000; $y <= date('Y'); $y++){
            echo '<option value="'.$y.'" '.($y == date('Y')?'selected':'').'>'.$y.'</option>';
        }?>
        </select>
    </div>
        <?php echo empty(form_error('year'))?"<br>":form_error('year'); ?><br>

        <label>ダウンロード対象月</label><br>
        <div class="select">
        <select name="month" required>
            <option value="01" <?php echo ('01' == date('m'))?'selected':'';?> >1</option>
            <option value="02" <?php echo ('02' == date('m'))?'selected':'';?> >2</option>
            <option value="03" <?php echo ('03' == date('m'))?'selected':'';?> >3</option>
            <option value="04" <?php echo ('04' == date('m'))?'selected':'';?> >4</option>
            <option value="05" <?php echo ('05' == date('m'))?'selected':'';?> >5</option>
            <option value="06" <?php echo ('06' == date('m'))?'selected':'';?> >6</option>
            <option value="07" <?php echo ('07' == date('m'))?'selected':'';?> >7</option>
            <option value="08" <?php echo ('08' == date('m'))?'selected':'';?> >8</option>
            <option value="09" <?php echo ('09' == date('m'))?'selected':'';?> >9</option>
            <option value="10" <?php echo ('10' == date('m'))?'selected':'';?> >10</option>
            <option value="11" <?php echo ('11' == date('m'))?'selected':'';?> >11</option>
            <option value="12" <?php echo ('12' == date('m'))?'selected':'';?> >12</option>
        </select>
    </div>
        <?php echo empty(form_error('month'))?"<br>":form_error('month'); ?>
    </div>
    <div class="conf2">
        <input class="send" type="submit" value="Send">
        </div>
    <?php echo form_close(); ?>
</div>

<?php $this->load->view('eapi/common/result.php', isset($result)?$result:array());?>

</body>
</html>