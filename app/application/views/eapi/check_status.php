<?php $this->load->view('eapi/common/header.php');?>
<?php $this->load->view('eapi/common/sidebar.php');?>

<div class="title">処理状況確認API</div>
<div class="conf">
    <?php echo form_open_multipart(base_url('eapi/check_status/exec'), array('method' => 'post'));?>
    <div class="conf1">
        <label>企業コード</label><br>
        <input type="text" name="company_code" value="" required>
        <?php echo empty(form_error('company_code'))?"<br>":form_error('company_code'); ?><br>

        <label>インポートID</label><br>
        <input type="text" name="import_id" value="" required>
        <?php echo empty(form_error('import_id'))?"<br>":form_error('import_id'); ?><br>

        <label>インポートタイプ</label><br>
        <input type="text" name="import_type" value="" required>
        <?php echo empty(form_error('import_type'))?"<br>":form_error('import_type'); ?>
    </div>
    <div class="conf2">
        <input class="send" type="submit" value="Send">
        </div>
    <?php echo form_close(); ?>
</div>

<?php $this->load->view('eapi/common/result.php', isset($result)?$result:array());?>

</body>
</html>