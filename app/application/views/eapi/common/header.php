<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 iegroup" lang="ja"> <![endif]-->
<!--[if IE 9]><html class="ie9 iegroup" lang="ja"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>HRTech-API-TestTool</title>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('static/icons/favicon16x16.ico');?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('static/icons/favicon32x32.ico');?>">
        <link rel="icon" type="image/png" sizes="48x48" href="<?php echo base_url('static/icons/favicon48x48.ico');?>">
        <link rel="icon" type="image/png" sizes="64x64" href="<?php echo base_url('static/icons/favicon64x64.ico');?>">
        <link rel="stylesheet" href="<?php echo base_url('static/css/base.css');?>">
    </head>
    <body>
        <div class="top">
            <div class="top1"><h1>EnigmaAPI テストツール</h1></div>
            <div class="top2">APIリクエスト先：<?php echo $this->config->item('request_server_name', 'api_config');?></div>
        </div>