<?php $this->load->view('eapi/common/header.php');?>
<?php $this->load->view('eapi/common/sidebar.php');?>

<div class="title">従業員連携API</div>
<div class="conf">
  <?php echo form_open_multipart(base_url('eapi/employee_import/exec'), array('method' => 'post'));?>
    <div class="conf1">
      <label>企業コード</label><br>
      <input type="text" name="company_code" value="" required>
      <?php echo empty($error = form_error('company_code', '<p class="error">', '</p>'))?"<br>":$error; ?><br>

      <label>従業員CSV</label><br>
      <input type="file" name="employee_csv" value="" required>
      <?php echo empty(form_error('employee_csv'))?"<br>":form_error('employee_csv'); ?>
  </div>
  <div class="conf2">
      <input class="send" type="submit" value="Send">
      </div>
  <?php echo form_close(); ?>
</div>

<?php $this->load->view('eapi/common/result.php', isset($result)?$result:array());?>

</body>
</html>