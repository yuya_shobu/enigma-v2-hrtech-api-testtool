<?php $this->load->view('eapi/common/header.php');?>
<?php $this->load->view('eapi/common/sidebar.php');?>

<div class="title">勤怠連携API</div>
<div class="conf">
  <?php echo form_open_multipart(base_url('eapi/kintai_import/exec'), array('method' => 'post'));?>
  <div class="conf1">
        <label>企業コード</label><br>
        <input type="text" name="company_code" value="" required>
        <?php echo empty(form_error('company_code'))?"<br>":form_error('company_code'); ?><br>

        <label>勤怠CSV</label><br>
        <input type="file" name="kintai_csv" value="" required>
        <?php echo empty(form_error('kintai_csv'))?"<br>":form_error('kintai_csv'); ?>
    </div>
    <div class="conf2">
        <input class="send" type="submit" value="Send">
        </div>
    <?php echo form_close(); ?>
</div>
<?php $this->load->view('eapi/common/result.php', isset($result)?$result:array());?>

</body>
</html>