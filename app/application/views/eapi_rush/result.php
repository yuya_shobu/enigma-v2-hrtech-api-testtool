<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 iegroup" lang="ja"> <![endif]-->
<!--[if IE 9]><html class="ie9 iegroup" lang="ja"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>HRTech-API-TestTool</title>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('static/icons/favicon16x16.ico');?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('static/icons/favicon32x32.ico');?>">
        <link rel="icon" type="image/png" sizes="48x48" href="<?php echo base_url('static/icons/favicon48x48.ico');?>">
        <link rel="icon" type="image/png" sizes="64x64" href="<?php echo base_url('static/icons/favicon64x64.ico');?>">
        <link rel="stylesheet" href="<?php echo base_url('static/css/base.css');?>">
    </head>
    <body>

<div class="conf">
  <?php echo form_open_multipart(base_url('eapi_rush/result/show'), array('method' => 'post'));?>
  <div class="conf1">
        <label>テストタイトル</label><br>
        <select name="test_title" required>
        <?php foreach($result_list as $dir){
            echo '<option value="'.$dir.'" '.(($test_title == $dir)?'selected':'').'>'.html_escape($dir).'</option>';
        }?>
        </select>
    </div>
    <div class="conf2">
        <input class="send" type="submit" value="Send">
        </div>
    <?php echo form_close(); ?>
</div>
<div class="res">
    <?php if(isset($result_json)){
        echo "<label>結果</label><br><pre>";
        echo '<br>'.var_dump($result_json).'</pre>';
    }?>
</div>

</div>

</body>
</html>