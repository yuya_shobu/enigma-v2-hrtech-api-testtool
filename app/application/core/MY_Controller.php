<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->config('api_config', true);
    }

    protected function _check_company_code(){
        if(!isset($_POST["company_code"])){
            log_message('error', 'notice:no company code');
            return false;
        }else{
            return $_POST["company_code"];
        }
    }

    protected function _get_token($code){
        $config = $this->config->item('jwt', 'api_config');
        $config['sub'] = $code;
        $token = $this->_encode($config, $this->config->item('jwt_secret', 'api_config'), true);
        return $token;
    }

    protected function _get_header($token){
        return array(
                        'Authorization: Bearer '.$token,
                        'Language: en',
                        'Accept: application/json',
                        //'alg: HS256',
                        //'typ: jwt'
                    );
    }

    protected function _encode($payload, $key){
        $header = array(
            'typ' => 'JWT',
            'alg' => $this->config->item('jwt_alg', 'api_config')
        );
        $segments = array();

        $segments[] = $this->_base64_encode($this->_json_encode($header));
        $segments[] = $this->_base64_encode($this->_json_encode($payload));
        $signing_input = implode('.', $segments);

        $signature = hash_hmac($this->config->item('jwt_enc_alg', 'api_config'), $signing_input, $key, true);
        $segments[] = $this->_base64_encode($signature);

        return implode('.', $segments);
    }

    protected function _base64_encode($input){
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    protected function _json_encode($input){
        $json = json_encode($input, JSON_UNESCAPED_SLASHES);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            static::handleJsonError($errno);
        } elseif ($json === 'null' && $input !== null) {
            throw new DomainException('Null result with non-null input');
        }
        return $json;
    }

    protected function _set_cookie_env(){
        $this->load->helper('cookie');
        $cookie_env = get_cookie('env');
        if($cookie_env){
            return $cookie_env;
        }else{

        }
    }

    protected function _set_api_server(){

        $get_env = $this->input->get('env', true);
        $cookie_env = get_cookie('env');

        $api_server_list = $this->config->item('api_server', 'api_config');

        if(isset($api_server_list[$get_env])){
            $this->api_server = $api_server_list[$get_env];
        }elseif(isset($api_server_list[$cookie_env])){
            $this->api_server = $api_server_list[$cookie_env];
        }else{

        }
    }

    protected function _get_csv($file_path){
        if (($fp = fopen($file_path, "r")) === false) {
            echo 'ERROR : request list is invalid';
            exit;
        }

        setlocale(LC_ALL, 'ja_JP');

        $i=0;
        while (($line = fgetcsv($fp)) !== FALSE) {
            mb_convert_variables('UTF-8', 'sjis-win', $line);
            $data[] = $line;
        }

        fclose($fp);

        return $data;
    }

    protected function _unzip($zip_path, $unzip_dir){

        $zip = new ZipArchive();

        $res = $zip->open($zip_path);

        if($res){
            $zip->extractTo($unzip_dir);
            $zip->close();
        }else{
            echo 'ERROR : archive csv file is invalid';
            exit;
        }

        return $res;
    }

    protected function _array_divide($array, $division){
        $count = ceil(count($array)/$division);
        return array_chunk($array, $count);
    }

    protected function _diff_microtime($end, $start){
        list($end_m, $end_t) = explode(' ', $end);
        list($start_m, $start_t) = explode(' ', $start);
        return ((float)$end_m-(float)$start_m) + ((float)$end_t-(float)$start_t);
    }

    protected function _check_pass($pass){

        if($pass === $this->config->item('rush_pass', 'api_config')){
            return true;
        }else{
            show_404();
        }
    }

}