<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//======== Base Settings ========

$config['api_server'] = "https://hrtechapi.test.plus.enigma.bz/";
$config['request_server_name'] = 'テスト環境';

/*
$config['api_server'] = array(
    'tst'  => array(
        'domain'    => 'https://hrtechapi.test.plus.enigma.bz/',
        'name'      => 'テスト環境'
    ),
    'dev'   => array(
        'domain'    => 'https://hrtechapi.dev.plus.enigma.bz/',
        'name'      => '開発環境'
    )
);
*/

$iat = time();

$config['jwt'] = array(
    'aud'       => 'hrtech',
    'sub'       => '',
    'iss'       => $config['api_server'],
    'iat'       => $iat,
    'exp'       => $iat+300,
);

$config['jwt_alg'] = "HS256";

$config['jwt_enc_alg'] = "SHA256";

$config['jwt_secret'] = "rF3fzgIkKepZiYSVoTzM58LFT6GyXmSo";

$config['cache_file_path'] = APPPATH.'../temp/cache/api';


//======== Rush test ========

$config['rush_pass'] = "wyjhGYvzmoP1ODOH";

$config['post_value'] = array(
    'employee_import'   => array(
        'passcode',
        'env',
        'thread_num',
        'title',
        'request_list',
        'archive_csv'
    ),
    'kintai_import'     => array(
        'passcode',
        'env',
        'thread_num',
        'title',
        'request_list',
        'archive_csv'
    ),
    'check_status'      => array(
        'passcode',
        'env',
        'thread_num',
        'title',
        'request_list'
    ),
    'download'     => array(
        'passcode',
        'env',
        'thread_num',
        'title',
        'request_list'
    )
);