<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kintai_import extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('eapi/kintai_import.php', array());
    }

    public function exec(){

        $this->_set_validate();

        $company_code = $this->input->post('company_code', true);

        $result = array(
            'res'   => '',
            'req'   => array(
                'code'      => "企業コード：".(empty($company_code)?'送信されていません。' : $company_code) ,
                'file_name' => "勤怠CSV：".(isset($_FILES['kintai_csv'])?$_FILES['kintai_csv']['name']:'送信されていません。'),
                'token_payload' => "tokenペイロード："
            )
        );

        if($this->form_validation->run() === true && isset($_FILES['kintai_csv'])){

            $token = $this->_get_token($company_code);

            $token_segment = explode('.', $token);
            $result['req']['token_payload'] .= base64_decode($token_segment[1]);

            $post_array = array(
                'headers'    => $this->_get_header($token),
                'body'      => '',
                'files' => array(
                    'tmp_name'  => $_FILES['kintai_csv']['tmp_name'],
                    'type'      => $_FILES['kintai_csv']['type'],
                    'name'      => $_FILES['kintai_csv']['name']
                ),
                'show_header'   => true
            );

            $res = $this->api->call('kintais-csv/imports', $post_array, 'FILE');

            $result['res'] = $res['response'];
        }

        $this->load->view('eapi/kintai_import.php', ['result' => $result]);

    }

    private function _set_validate(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_code', '企業コード', 'required');
        //$this->form_validation->set_rules('kintai_csv', '従業員CSV', 'required');
        $this->form_validation->set_message('required', '%sを入力してください。');
    }
}