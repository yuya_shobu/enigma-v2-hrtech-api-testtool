<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_status extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('eapi/check_status.php', array());
    }

    public function exec(){

        $this->_set_validate();

        $company_code = $this->input->post('company_code', true);
        $import_id = $this->input->post('import_id', true);
        $type = $this->input->post('import_type', true);

        $result = array(
            'res'   => '',
            'req'   => array(
                'code'      => "企業コード：".(empty($company_code)? '送信されていません。' : $company_code),
                'import_id' => "インポートID：".(empty($import_id)? '送信されていません。' : $import_id ),
                'type'      => "インポートタイプ：".(empty($type)? '送信されていません。' : $type),
                'token_payload' => "tokenペイロード："
            )
        );

        if($this->form_validation->run() === true){

            $token = $this->_get_token($company_code);

            $token_segment = explode('.', $token);
            $result['req']['token_payload'] .= base64_decode($token_segment[1]);

            $headers = $this->_get_header($token);

            $post_array = array(
                'headers'    => $headers,
                'body'      => array(
                    'import_id' => $import_id,
                    'type'      => $type
                ),
                'show_header'   => true
            );

            $res = $this->api->call('check-status/import', $post_array, 'POST');

            $result['res'] = $res['response'];

        }

        $this->load->view('eapi/check_status.php', ['result' => $result]);
    }

    private function _set_validate(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_code', '企業コード', 'required');
        $this->form_validation->set_rules('import_id', 'インポートID', 'required');
        $this->form_validation->set_rules('import_type', 'インポートタイプ', 'required');
        $this->form_validation->set_message('required', '%sを入力してください。');
    }
}