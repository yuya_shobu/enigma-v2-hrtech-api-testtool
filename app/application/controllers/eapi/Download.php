<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
    $this->load->view('eapi/download.php', array());
    }

    public function exec(){

        $this->_set_validate();

        $company_code = $this->input->post('company_code', true);
        $year = $this->input->post('year', true);
        $month = $this->input->post('month', true);

        $result = array(
            'res'   => '',
            'req'   => array(
                'code'      => "企業コード：".(empty($company_code)? '送信されていません。' : $company_code),
                'year'      => "ダウンロード対象年：".(empty($year)? '送信されていません。' : $year),
                'month'     => "ダウンロード対象月：".(empty($month)? '送信されていません。' : $month),
                'token_payload' => "tokenペイロード："
            )
        );

        if($this->form_validation->run() === true){

            $token = $this->_get_token($company_code);

            $token_segment = explode('.', $token);
            $result['req']['token_payload'] .= base64_decode($token_segment[1]);

            $post_array = array(
                'headers'    => $this->_get_header($token),
                'body'      => array(
                    'year_month' => $year.'-'.$month
                ),
                'dl'        => true,
                'show_header'   => true
            );

            $res = $this->api->call('branch-payment-requests/export-csv', $post_array, 'GET');

            $result['res'] = $res['response'];
        }

        $this->load->view('eapi/download.php', ['result' => $result]);
    }

    private function _set_validate(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_code', '企業コード', 'required');
        $this->form_validation->set_rules('year', '年', 'required');
        $this->form_validation->set_rules('month', '月', 'required');
        $this->form_validation->set_message('required', '%sを入力してください。');
    }
}