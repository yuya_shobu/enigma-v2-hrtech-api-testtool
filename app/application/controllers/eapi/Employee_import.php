<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(dirname(__FILE__)."/Commons.php");

class Employee_import extends Commons {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('eapi/employee_import.php', array());
    }

    public function exec(){

        $this->_set_validate();

        $code = $this->input->post('company_code', true);

        $result = array(
            'res'   => '',
            'req'   => array(
                'code'      => "企業コード：".(empty($code)? '送信されていません。' : $code),
                'file_name' => "従業員CSV：".(isset($_FILES['employee_csv'])?$_FILES['employee_csv']['name']:'送信されていません。'),
                'token_payload' => "tokenペイロード："
            )
        );

        if($this->form_validation->run() === true && isset($_FILES['employee_csv'])){

            $token = $this->_get_token($code);

            $token_segment = explode('.', $token);
            $result['req']['token_payload'] .= base64_decode($token_segment[1]);

            $post_array = array(
                'headers'   => $this->_get_header($token),
                'body'      => '',
                'files'     => array(
                    'tmp_name'  => $_FILES['employee_csv']['tmp_name'],
                    'type'      => $_FILES['employee_csv']['type'],
                    'name'      => $_FILES['employee_csv']['name']
                ),
                'show_header'   => true
            );

            $res = $this->api->call('imports/employee', $post_array, 'FILE');

            $result['res'] = $res['response'];
        }

        $this->load->view('eapi/employee_import.php', ['result' => $result]);

    }

    private function _set_validate(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_code', '企業コード', 'required');
        //$this->form_validation->set_rules('employee_csv', '従業員CSV', 'required');
        $this->form_validation->set_message('required', '%sを入力してください。');
    }
}