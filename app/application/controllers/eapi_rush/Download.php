<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'controllers/eapi_rush/Commons.php');

const API_TYPE = 'download';
const API_URL  = 'branch-payment-requests/export-csv';

class Download extends Commons {

    public function __construct(){
        parent::__construct();
        $this->load->helper('file');
        $this->api_type = API_TYPE;
        $this->api_url = API_URL;
    }

    public function index(){
        $this->_init();
    }

    public function fork(){
        $this->_do_fork_get();
    }
}