<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commons extends MY_Controller {

    function __construct(){
        parent::__construct();
    }

    protected function _request_check($check_list = array()){

        $this->post = $this->input->post(NULL, true);

        foreach($check_list as $check_item){
            $func = '_'.$check_item;
            if(method_exists($this, $func)){
                $this->$func($this->post);
            }
        }
    }

    protected function _get_archive_tmp_path($title){
        return APPPATH.'tmp/'.$title.'/';
    }

    protected function _init(){

        $post_value = $this->config->item('post_value', 'api_config');

        //バリデーション&エラーレスポンス
        $this->_request_check($post_value[$this->api_type]);

        //テストタイトル定義
        if(!isset($this->post['title'])){
            $this->post['title'] = $this->api_type;
        }
        $title = $this->post['title'].date( "_Ymd-His" );

        //アーカイブディレクトリ定義・作成
        $this->_define_log_dir($title, true);

        //プロセスごとのtmpファイルリスト作成
        $this->_set_tmp_filelist();

        // 処理開始時間記録
        if(!write_file($this->log_path.'start.json', json_encode(['start_time'=>microtime()]))){
            //log_message('error', 'Failed sliced request file creation.');
        }

        for ($i=1; $i<=$this->post['thread_num']; $i++) {
            $command = 'curl -F "env='.$this->post['env'].'" -F "thread_max='.$this->post['thread_num'].'" -F "thread_no='.$i.'" -F "dir='.$title.'" '.base_url().'eapi_rush/'.$this->api_type.'/fork > /dev/null &';
            if(isset($this->post['test_mode'])&&$this->post['test_mode'] === 'local'){
                echo'<pre>';var_dump( $command );exit;
            }
            exec($command);
        }
        echo "Process Start (".$this->post['thread_num'].")";

    }

    protected function _do_fork_get(){
        $env = $this->input->post('env', true);
        $thread_max = $this->input->post('thread_max', true);
        $thread_no = $this->input->post('thread_no', true);
        $dir = $this->input->post('dir', true);

        //アーカイブディレクトリ定義・作成
        $this->_define_log_dir($dir);

        $request_file = $this->tmp_path.'thread_'.$thread_no.'.json';

        if(!($request_json = read_file($request_file))){
            log_message('error', 'ERROR: Request file not found');
        }

        $request_array = json_decode($request_json, true);

        $result = array();
        $request_log = '';

        foreach($request_array as $req){

            $token = $this->_get_token($req['company_code']);

            $get_array = array(
                'headers'   => $this->_get_header($token),
                'body'      => array(
                    'year_month' => $req['year_month']
                ),
                'api_server'     => $env,
                'request_log'   => true
            );

            $res = $this->api->call($this->api_url, $get_array, 'GET');

            $response_json = json_decode($res['response'], true);

            if($response_json != NULL){
                $response = array_merge(['company_code' => $req['company_code']], $response_json);
            }else{
                $response = array(
                    'company_code' => $req['company_code'],
                    'status_code'          => 200
                );
            }

            $result[] = $response;

            $request_log .= 'Company-Code: '.$req['company_code']."\n".
                            'Date: '.date('Y/m/d H-i-s')."\n".
                            $res['request']."\n".
                            '===================================================='."\n";
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'.json', json_encode($result))){
            log_message('error', 'json Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'_request.log', $request_log)){
            log_message('error', 'Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        $continue = true;
        $thread_num = 1;
        while($thread_num <= $thread_max && $continue){
            $json_log_path = $this->log_path.'thread_'.$thread_num.'.json';
            if(!file_exists($json_log_path)){
                $continue = false;
            }
            $thread_num++;
        }

        if($continue){

            $end_time = microtime();

            $log_file = array();
            $log_req_file = '';
            $successes = array();
            $errors = array();

            $thread_result_array = array();

            for ($i=1; $i<=$thread_max; $i++) {
                $json_log_path = $this->log_path.'thread_'.$i.'.json';
                $req_log_path  = $this->log_path.'thread_'.$i.'_request.log';
                if(file_exists($json_log_path) && file_exists($req_log_path)){
                    $thread_result_array = array_merge(json_decode(read_file($json_log_path), true), $thread_result_array);
                    $log_req_file .= read_file($req_log_path);
                }else{
                    log_message('error', 'Log file is not found : result@'.$json_log_path.' request@'.$req_log_path);
                    exit;
                }
            }

            foreach($thread_result_array as $thread_result){
                if($thread_result['status_code'] == 200 || $thread_result['status_code'] == 201 || $thread_result['status_code'] == 202){
                    $successes[] = $thread_result;
                }else{
                    $errors[] = $thread_result;
                }
            }

            $start_time = json_decode(read_file(APPPATH.'logs/rush_result/'.$dir.'/start.json'), true);

            $success = count($successes);
            $error = count($errors);
            $result_array = array(
                'ALL'       => $success + $error,
                'THREADS'    => $thread_max,
                'PROCESSING TIME' => $this->_diff_microtime($end_time, $start_time['start_time']).'s',
                'SUCCESS'   => $success,
                'ERROR'     => $error
            );

            $response_array = array(
                'SUCCESS'   => $successes,
                'ERRORS'  => $errors
            );

            if(!write_file($this->log_path.'result.json', json_encode($result_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'response.json', json_encode($response_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'request.log', $log_req_file)){
                echo 'error';exit;
            }

        }
    }

    protected function _do_fork_post(){
        $env = $this->input->post('env', true);
        $thread_max = $this->input->post('thread_max', true);
        $thread_no = $this->input->post('thread_no', true);
        $dir = $this->input->post('dir', true);

        //アーカイブディレクトリ定義・作成
        $this->_define_log_dir($dir);

        $request_file = $this->tmp_path.'thread_'.$thread_no.'.json';

        if(!($request_json = read_file($request_file))){
            log_message('error', 'ERROR: Request file not found');
        }

        $request_array = json_decode($request_json, true);

        $result = array();
        $request_log = '';

        foreach($request_array as $req){

            $token = $this->_get_token($req['company_code']);

            $post_array = array(
                'headers'   => $this->_get_header($token),
                'body'      => array(
                    'import_id' => $req['import_id'],
                    'type'      => $req['import_type']
                ),
                'api_server'     => $env,
                'request_log'   => true
            );

            $res = $this->api->call($this->api_url, $post_array, 'POST');

            $response_json = json_decode($res['response'], true);

            if($response_json != NULL){
                $response = array_merge(['company_code' => $req['company_code']], $response_json);
            }else{
                $response = array(
                    'company_code' => $req['company_code'],
                    'status_code'  => 400,
                    'error'     => 'failed to decode json',
                    'data'  => ['row'=>$res['response']]
                );
            }

            $result[] = $response;

            $request_log .= 'Company-Code: '.$req['company_code']."\n".
                            'Date: '.date('Y/m/d H-i-s')."\n".
                            $res['request']."\n".
                            '===================================================='."\n";
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'.json', json_encode($result))){
            log_message('error', 'json Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'_request.log', $request_log)){
            log_message('error', 'Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        $continue = true;
        $thread_num = 1;
        while($thread_num <= $thread_max && $continue){
            $json_log_path = $this->log_path.'thread_'.$thread_num.'.json';
            if(!file_exists($json_log_path)){
                $continue = false;
            }
            $thread_num++;
        }

        if($continue){

            $end_time = microtime();

            $log_file = array();
            $log_req_file = '';
            $successes = array();
            $errors = array();

            $thread_result_array = array();

            for ($i=1; $i<=$thread_max; $i++) {
                $json_log_path = $this->log_path.'thread_'.$i.'.json';
                $req_log_path  = $this->log_path.'thread_'.$i.'_request.log';
                if(file_exists($json_log_path) && file_exists($req_log_path)){
                    $thread_result_array = array_merge(json_decode(read_file($json_log_path), true), $thread_result_array);
                    $log_req_file .= read_file($req_log_path);
                }else{
                    log_message('error', 'Log file is not found : result@'.$json_log_path.' request@'.$req_log_path);
                    exit;
                }
            }

            foreach($thread_result_array as $thread_result){
                if($thread_result['status_code'] == 200 || $thread_result['status_code'] == 201 || $thread_result['status_code'] == 202){
                    $successes[] = $thread_result;
                }else{
                    $errors[] = $thread_result;
                }
            }

            $start_time = json_decode(read_file(APPPATH.'logs/rush_result/'.$dir.'/start.json'), true);

            $success = count($successes);
            $error = count($errors);
            $result_array = array(
                'ALL'       => $success + $error,
                'THREADS'    => $thread_max,
                'PROCESSING TIME' => $this->_diff_microtime($end_time, $start_time['start_time']).'s',
                'SUCCESS'   => $success,
                'ERROR'     => $error
            );

            $response_array = array(
                'SUCCESS'   => $successes,
                'ERRORS'  => $errors
            );

            if(!write_file($this->log_path.'result.json', json_encode($result_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'response.json', json_encode($response_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'request.log', $log_req_file)){
                echo 'error';exit;
            }

        }
    }

    protected function _do_fork_file(){
        $env = $this->input->post('env', true);
        $thread_max = $this->input->post('thread_max', true);
        $thread_no = $this->input->post('thread_no', true);
        $dir = $this->input->post('dir', true);

        //アーカイブディレクトリ定義・作成
        $this->_define_log_dir($dir);

        $request_file = $this->tmp_path.'thread_'.$thread_no.'.json';

        if(!($request_json = read_file($request_file))){
            log_message('error', 'ERROR: Request file not found');
        }

        $request_array = json_decode($request_json, true);

        $result = array();
        $request_log = '';

        foreach($request_array as $req){

            $token = $this->_get_token($req['company_code']);

            if(isset($req['mode']) && $req['mode'] == 'test'){
                $tmp_name = APPPATH.'content/'.$req['request_csv'];
            }else{
                $tmp_name = APPPATH.'tmp/'.$dir.'/'.$req['request_csv'];
            }

            if(!file_exists($tmp_name)){
                log_message('error', 'ERROR: Request tmp file not found');
                exit;
            }

            $files = array(
                'tmp_name'  => $tmp_name,
                'type'      => 'text/csv',
                'name'      => $req['request_csv']
            );

            $post_array = array(
                'headers'   => $this->_get_header($token),
                'body'      => '',
                'files'     => $files,
                'api_server'     => $env,
                'request_log'   => true
            );

            $res = $this->api->call($this->api_url, $post_array, 'FILE');

            $response_json = json_decode($res['response'], true);

            if($response_json != NULL){
                $response = array_merge(['company_code' => $req['company_code']], $response_json);
            }else{
                $response = array(
                    'company_code' => $req['company_code'],
                    'status_code'  => 400,
                    'error'     => 'failed to decode json',
                    'data'  => ['row'=>$res['response']]
                );
            }

            $result[] = $response;

            $request_log .= 'Company-Code: '.$req['company_code']."\n".
                            'Date: '.date('Y/m/d H-i-s')."\n".
                            $res['request']."\n".
                            '===================================================='."\n";
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'.json', json_encode($result))){
            log_message('error', 'json Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        if(!write_file($this->log_path.'thread_'.$thread_no.'_request.log', $request_log)){
            log_message('error', 'Log file creation failed：title@'.$dir.' thread@'.$thread_no);
            exit;
        }

        $continue = true;
        $thread_num = 1;
        while($thread_num <= $thread_max && $continue){
            $json_log_path = $this->log_path.'thread_'.$thread_num.'.json';
            if(!file_exists($json_log_path)){
                $continue = false;
            }
            $thread_num++;
        }

        if($continue){

            $end_time = microtime();

            $log_file = array();
            $log_req_file = '';
            $successes = array();
            $errors = array();

            $thread_result_array = array();

            for ($i=1; $i<=$thread_max; $i++) {
                $json_log_path = $this->log_path.'thread_'.$i.'.json';
                $req_log_path  = $this->log_path.'thread_'.$i.'_request.log';
                if(file_exists($json_log_path) && file_exists($req_log_path)){
                    $thread_result_array = array_merge(json_decode(read_file($json_log_path), true), $thread_result_array);
                    $log_req_file .= read_file($req_log_path);
                }else{
                    log_message('error', 'Log file is not found : result@'.$json_log_path.' request@'.$req_log_path);
                    exit;
                }
            }

            foreach($thread_result_array as $thread_result){
                if($thread_result['status_code'] == 200 || $thread_result['status_code'] == 201 || $thread_result['status_code'] == 202){
                    $successes[] = $thread_result;
                }else{
                    $errors[] = $thread_result;
                }
            }

            $start_time = json_decode(read_file(APPPATH.'logs/rush_result/'.$dir.'/start.json'), true);

            $success = count($successes);
            $error = count($errors);
            $result_array = array(
                'ALL'       => $success + $error,
                'THREADS'    => $thread_max,
                'PROCESSING TIME' => $this->_diff_microtime($end_time, $start_time['start_time']).'s',
                'SUCCESS'   => $success,
                'ERROR'     => $error
            );

            $response_array = array(
                'SUCCESS'   => $successes,
                'ERRORS'  => $errors
            );

            if(!write_file($this->log_path.'result.json', json_encode($result_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'response.json', json_encode($response_array))){
                echo 'error';exit;
            }
            if(!write_file($this->log_path.'request.log', $log_req_file)){
                echo 'error';exit;
            }

        }
    }

    protected function _rmrf($dir) {
        if (is_dir($dir) and !is_link($dir)) {
            array_map('_rmrf',   glob($dir.'/*', GLOB_ONLYDIR));
            array_map('unlink', glob($dir.'/*'));
            rmdir($dir);
        }
    }

    protected function _define_log_dir($title, $mkdir=false){
        $this->tmp_path = APPPATH.'tmp/rush_request/'.$title.'/';
        $this->log_path = APPPATH.'logs/rush_result/'.$title.'/';
        if($mkdir){
            if(!mkdir($this->tmp_path, 0777) || !mkdir($this->log_path, 0777)){
                echo "ERROR : Failed archive directly creation";
                exit;
            }
        }
    }

    protected function _set_tmp_filelist(){
        $request_array = $this->_get_csv($this->post['request_list']['tmp_name']);
        $request_list = array();

        if(isset($this->post['archive_csv']) && is_array($this->post['archive_csv'])){
            echo "It is not supported with zip files";
            exit;
            /*
            $this->_unzip($this->post['archive_csv']['tmp_name'], $this->archive_tmp_path);
            foreach($request_array as $request_data){

                $employee_csv_file_path = $this->archive_tmp_path.$request_data[1].'.csv';
                if(file_exists($employee_csv_file_path)){
                    $request_list[] = array(
                        'company_code'  => $request_data[0],
                        'employee_csv'  => $request_data[1].'.csv'
                    );
                }else{
                    echo "ERROR : Requested csv file does not exist. company_code@".$request_data[0].' employee_csv@'.$request_data[1].'.csv';
                    exit;
                }
            }
            */
        }else{
            $set_request_func = "_set_request_".$this->api_type;
            foreach($request_array as $request_data){
                $request_list[] = $this->$set_request_func($request_data);
            }
        }

        $request_list_sliced = $this->_array_divide($request_list, $this->post['thread_num']);

        foreach($request_list_sliced as $thread => $sliced_request){
            $thread++;
            if(!write_file($this->tmp_path.'thread_'.$thread.'.json', json_encode($sliced_request, JSON_UNESCAPED_SLASHES))){
                echo 'ERROR : Failed sliced request file creation.';
                exit;
            }
        }
    }

    private function _set_request_employee_import($request_data){
        return array(
            'company_code'  => $request_data[0],
            'request_csv'   => $this->post['archive_csv'].'.csv',
            'mode'          => 'test'
        );
    }

    private function _set_request_kintai_import($request_data){
        return array(
            'company_code'  => $request_data[0],
            'request_csv'   => $this->post['archive_csv'].'.csv',
            'mode'          => 'test'
        );
    }

    private function _set_request_check_status($request_data){
        return array(
            'company_code'  => $request_data[0],
            'import_id'   => $request_data[1],
            'import_type'          => $request_data[2]
        );
    }

    private function _set_request_download($request_data){
        return array(
            'company_code'  => $request_data[0],
            'year_month'   => $request_data[1].'-'.str_pad($request_data[2], 2, 0, STR_PAD_LEFT)
        );
    }

    private function _passcode($post){
        if(!isset($post['passcode'])){
            echo 'ERROR : passcode is required';
            exit;
        }elseif(!$this->_check_pass($post['passcode'])){
            echo 'ERROR : passcode is wrong';
            exit;
        }
    }

    private function _env($post){
        if(!isset($post['env'])){
            echo 'ERROR : env value not found';
            exit;
        }elseif($post['env'] != 'stg' && $post['env'] != 'tst' && $post['env'] != 'dev' ){
            echo 'ERROR : env value is invalid';
            exit;
        }
    }

/*
    private function _request_num($post){
        if(!isset($post['request_num'])){
            echo 'ERROR : request number not found';
            exit;
        }elseif(!is_numeric($post['request_num'])){
            echo 'ERROR : request number is invalid value. Please enter with numbers';
            exit;
        }
    }
*/

    private function _thread_num($post){
        if(!isset($post['thread_num'])){
            echo 'ERROR : thread number not found';
            exit;
        }elseif(!is_numeric($post['thread_num'])){
            echo 'ERROR : thread number is invalid value. Please enter with numbers';
            exit;
        }
    }

    private function _title($post){}

    private function _request_list($post){
        if(isset($_FILES['request_list'])){
            $this->post['request_list'] = $_FILES['request_list'];
        }else{
            echo 'ERROR : request list file not found';
            exit;
        }
    }

    private function _archive_csv($post){
        if(isset($_FILES['archive_csv'])){
            $this->post['archive_csv'] = $_FILES['archive_csv'];
        }elseif(isset($post['archive_csv'])){
            $this->post['archive_csv'] = $post['archive_csv'];
        }else{
            echo 'ERROR : archive csv file not found';
            exit;
        }
    }

}