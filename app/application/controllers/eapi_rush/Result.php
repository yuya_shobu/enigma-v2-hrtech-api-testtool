<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->log_path = APPPATH.'logs/rush_result/';
    }

    public function index(){
        $array = array(
            'test_title'    => '',
            'result_list'   => $this->_get_dir_list(),
            'result_json'   => ''
        );
        $this->load->view('eapi_rush/result.php', $array);
    }

    public function show(){


        $test_title = $this->input->post('test_title', true);

        $result_path = $this->log_path.$test_title.'/result.json';
        $response_path = $this->log_path.$test_title.'/response.json';

        $result_json = array();
        if(file_exists($result_path)){
            $this->load->helper('file');
            $result_json[] = json_decode(read_file($result_path), true);
        }
        if(file_exists($response_path)){
            $this->load->helper('file');
            $result_json[] = json_decode(read_file($response_path), true);
        }


        $array = array(
            'test_title'    => $test_title,
            'result_list'   => $this->_get_dir_list(),
            'result_json'   => $result_json
        );

        $this->load->view('eapi_rush/result.php', $array);

    }

    private function _get_dir_list(){
        $dirs = scandir($this->log_path);

        $result_list = array();

        foreach($dirs as $dir){

            if(file_exists($this->log_path.$dir.'/result.json')){
                $result_list[] = $dir;
            }
        }

        return $result_list;
    }




}