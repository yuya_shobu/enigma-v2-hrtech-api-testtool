<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv_generator extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->log_path = APPPATH.'logs/rush_result/';
    }

    public function test(){


        $head = array(
            "本支店名",
            "従業員コード",
            "従業員名",
            "社会保険加入状況",
            "金融機関コード",
            "営業店番号",
            "預金種目",
            "口座番号",
            "口座名義"
        );
/*
        $head = array(
            "従業員コード",
            "支払支給額"
        );
*/
        $csv_array = array();
        $count = 1;
        while($count <=200000){

            $num = sprintf("%06d", $count);

            $csv_array[$count] = array(
                'rush test 001 branch 001',
                'e'.$num,
                'テ',
                '1',
                '1111',
                '123',
                '普通',
                '1234567',
                'TEST'
            );
/*

            $csv_array[$count] = array(
                'e'.$num,
                $count*100
            );
*/
            $count++;
        }

        $fp = fopen('php://memory', 'r+');

        fputcsv($fp, $head, ',', '"');
        foreach($csv_array as $csv_line){
            fputcsv($fp, $csv_line, ',', '"');
        }

        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=test".date('YmdHis').".csv");
        rewind($fp);
        print stream_get_contents($fp);
        fclose($fp);


    }

}